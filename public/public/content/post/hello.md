+++
Categories = ["code"]
Description = ""
Tags = []
date = "2015-09-19T11:16:18-04:00"
title = "hello world"

+++
Welcome to the Oldschool Systems blog. As I get things up and running, I figured
that a good Hello World post would be a "Hello World" example for [GoCZMQ](https://github.com/zeromq/goczmq/), the Golang binding for the [CZMQ](https://github.com/zeromq/czmq/) API.
```go
package main

import (
    "log"
    "github.com/zeromq/goczmq"
)

func main() {
    push, err := goczmq.NewPush("inproc://helloworld")
    if err != nil {
        log.Fatal(err)
    }

    pull, err := goczmq.NewPull("inproc://helloworld")
    if err != nil {
        log.Fatal(err)
    }

    err = push.SendFrame([]byte("Hello"), goczmq.FlagNone)
    if err != nil {
        log.Fatal(err)
    }

    msg, flag, err := pull.RecvFrame()
    if err != nil {
        log.Fatal(err)
    }

    log.Printf("Received msg '%s' with flag %d\n", msg, flag)    
}
```

