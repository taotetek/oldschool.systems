<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Posts on Oldschool Systems</title>
    <link>http://taotetek.github.io/oldschool.systems/post/index.xml</link>
    <description>Recent content in Posts on Oldschool Systems</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <lastBuildDate>Sun, 09 Oct 2016 08:56:11 -0400</lastBuildDate>
    <atom:link href="http://taotetek.github.io/oldschool.systems/post/index.xml" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Problem: My Friend Pieter is Dead</title>
      <link>http://taotetek.github.io/oldschool.systems/post/pieter2/</link>
      <pubDate>Sun, 09 Oct 2016 08:56:11 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/pieter2/</guid>
      <description>

&lt;h3 id=&#34;solution-carry-his-ideas-forward&#34;&gt;Solution: Carry his ideas forward.&lt;/h3&gt;

&lt;p&gt;It was March of 2016 when I got the news from Pieter that his cancer was back, and that it was terminal this time. Shortly after that in April, he wrote &lt;a href=&#34;http://hintjens.com/blog:115&#34;&gt;&amp;ldquo;A Protocol for Dying&amp;rdquo;&lt;/a&gt; and announced it to the world. He lived much longer than was first expected, dying on October 4th on &lt;a href=&#34;http://hintjens.com/blog:116&#34;&gt;&amp;ldquo;his own terms&amp;rdquo;&lt;/a&gt; once cancer made it impossible for him to live on his own terms. I am left with grief, wonderful memories, and membership in an amazing community of software developers that Pieter built.&lt;/p&gt;

&lt;p&gt;I want to write some about the ideals that Pieter held that I also share - most importantly I think, his fierce dedication to taking power away from gatekeepers, and his belief in the power of collective intelligence. Right now, I&amp;rsquo;m still too sad and tired to muster up the energy required - but I felt the need to write the bookend to my April post.&lt;/p&gt;

&lt;p&gt;If you want to know what Pieter stood for, read his books. You can find them for free on the internet, but if you can afford to buy them please do - the money from the sales will help support his three children.&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://www.amazon.com/Culture-Empire-Digital-Revolution-1/dp/1492999776&#34;&gt;&amp;ldquo;Culture and Empire&amp;rdquo;&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://www.amazon.com/Psychopath-Code-Cracking-Predators-Stalk/dp/1514342022/ref=sr_1_1?s=books&amp;amp;ie=UTF8&amp;amp;qid=1476019689&amp;amp;sr=1-1&amp;amp;keywords=the+psychopath+code&#34;&gt;&amp;ldquo;The Psycopath Code&amp;rdquo;&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://www.amazon.com/Social-Architecture-Building--line-Communities/dp/1533112452/ref=sr_1_1?s=books&amp;amp;ie=UTF8&amp;amp;qid=1476019722&amp;amp;sr=1-1&amp;amp;keywords=social+architecture&#34;&gt;&amp;ldquo;Social Architecture&amp;rdquo;&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://www.amazon.com/Confessions-Necromancer-stories-Pieter-Hintjens/dp/1539178846/ref=sr_1_2?s=books&amp;amp;ie=UTF8&amp;amp;qid=1476019750&amp;amp;sr=1-2&amp;amp;keywords=confessions+of+a+necromancer&#34;&gt;&amp;ldquo;Confessions of a Necromancer&amp;rdquo;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</description>
    </item>
    
    <item>
      <title>Problem: My Friend Pieter is Dying</title>
      <link>http://taotetek.github.io/oldschool.systems/post/pieter/</link>
      <pubDate>Sat, 30 Apr 2016 22:35:54 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/pieter/</guid>
      <description>

&lt;h3 id=&#34;solution-there-s-no-solution&#34;&gt;Solution: There&amp;rsquo;s no solution.&lt;/h3&gt;

&lt;p&gt;My friend Pieter Hintjens &lt;a href=&#34;http://hintjens.com/blog:115&#34;&gt;is dying&lt;/a&gt;. I&amp;rsquo;m going to miss him. It&amp;rsquo;s a truth in life that sometimes we are presented with problems that cannot be solved. This is one of them. Pieter is now facing the ultimate unsolvable problem - the one that we will all eventually face. I&amp;rsquo;m facing the problem of navigating a storming sea of thoughts and emotions that I&amp;rsquo;m experiencing as a person who knows him, and values his presence in my life, and is facing losing that presence. None of these thoughts and emotions really fit into words for me. This is a constant and different problem in my life - words always feel too small to me and stringing them together into sentences is so linear, slow, and inadequate.&lt;/p&gt;

&lt;p&gt;Pieter, I&amp;rsquo;ll always carry you with me. All of the colors and textures of the too few moments we got to spend together. Stumbling drunk through London within hours of conversation and laughter. Discussing ideas for ZeroMQ and thoughts about psycopathy in Brussels. Having the pleasure of introducing you to various friends. Music in the warehouse.&lt;/p&gt;

&lt;p&gt;It was good, my friend. Every moment was good. Thank you for the moments shared. All of them are kept, and treasured. Your gestures will carry their momentum beyond you through those who understand what you were trying to say with ZeroMQ, and we&amp;rsquo;ll keep showing others.&lt;/p&gt;

&lt;p&gt;It has been an honor and a rare pleasure.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Problem: I want to use ZeroMQ with Go</title>
      <link>http://taotetek.github.io/oldschool.systems/post/goczmq1/</link>
      <pubDate>Sat, 26 Sep 2015 16:57:35 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/goczmq1/</guid>
      <description>

&lt;h3 id=&#34;solution-goczmq-https-github-com-zeromq-goczmq&#34;&gt;Solution: &lt;a href=&#34;https://github.com/zeromq/goczmq&#34;&gt;GoCZMQ&lt;/a&gt;&lt;/h3&gt;

&lt;h4 id=&#34;background&#34;&gt;Background&lt;/h4&gt;

&lt;p&gt;&lt;a href=&#34;http://zeromq.org&#34;&gt;ZeroMQ&lt;/a&gt; is a distributed messaging library focused on efficiency and simplicity. &lt;a href=&#34;http://czmq.zeromq.org&#34;&gt;CZMQ&lt;/a&gt; is a high level C binding for ZeroMQ. It provides a clean API across multiple versions of ZeroMQ that additionally provides a suite of useful services. I have been using ZeroMQ and CZMQ in projects for several years and occassionally contributing to CZMQ and other ZeroMQ related projects.  When I first started experimenting with Go, I felt its focus on simple concurrency was a good match with the guiding philosophy behind ZeroMQ.&lt;/p&gt;

&lt;p&gt;While &lt;a href=&#34;https://github.com/pebbe&#34;&gt;Pebbe&amp;rsquo;s&lt;/a&gt; Go bindings for ZeroMQ were available at the time and looked to be high quality, I decided writing a Go language binding for CZMQ would be a fun way to learn about Go&amp;rsquo;s &lt;a href=&#34;https://golang.org/cmd/cgo/&#34;&gt;cgo&lt;/a&gt; interface to C.&lt;/p&gt;

&lt;p&gt;I decided to develop the bindings as a ZeroMQ organization project using the &lt;a href=&#34;http://rfc.zeromq.org/spec:22&#34;&gt;Collective Code Construction Contract&lt;/a&gt; process. While I had contributed to ZeroMQ projects using this process, I had never started a project from scratch using it. I started the project with the pull request &amp;ldquo;&lt;a href=&#34;https://github.com/zeromq/goczmq/pull/1&#34;&gt;Problem: there is no documentation&lt;/a&gt;&amp;rdquo; on September 6th, 2014.&lt;/p&gt;

&lt;p&gt;Since then, I picked up a regular collaborator (hi Luna!) and five other other contributors. The API is stable, and myself and others are building projects on top of it. The experience has been great, and there are many things I could discuss about what I&amp;rsquo;ve learned on the way - but for now, let&amp;rsquo;s just answer the question &amp;ldquo;how do I use it?&amp;rdquo;.&lt;/p&gt;

&lt;h4 id=&#34;installation&#34;&gt;Installation&lt;/h4&gt;

&lt;p&gt;These instructions are for building GoCZMQ from git master. We&amp;rsquo;ll build it against CZMQ from git master, which is in turn built against ZeroMQ from git master. If you&amp;rsquo;re used to stable releases, this may seem odd. In ZeroMQ organization projects, we do not use development branches. We also don&amp;rsquo;t care much about traditional &amp;ldquo;stable&amp;rdquo; releases. Version tags are mostly to make packaging easier for OS maintainers who care about such things. This practice is codified in the &lt;a href=&#34;http://rfc.zeromq.org/spec:22&#34;&gt;C4.1 process documentation&lt;/a&gt;:&lt;/p&gt;

&lt;blockquote&gt;
&lt;p&gt;&amp;ldquo;The project SHALL have one branch (&amp;ldquo;master&amp;rdquo;) that always holds the latest in-progress version and SHOULD always build.&amp;rdquo;&lt;/p&gt;

&lt;p&gt;&amp;ldquo;The project SHALL NOT use topic branches for any reason. Personal forks MAY use topic branches.&amp;rdquo;&lt;/p&gt;
&lt;/blockquote&gt;

&lt;h5 id=&#34;libsodium&#34;&gt;libsodium&lt;/h5&gt;

&lt;p&gt;First, we need libsodium. Sodium is a &amp;ldquo;modern, easy-to-use software library for encryption, decryption, signatures, password hashing and more&amp;rdquo;. ZeroMQ relies on it for encryption. It is likely there are dev packages for it for your OS, but here are the instructions for building it just in case:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.3.tar.gz
wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.3.tar.gz.sig
wget https://download.libsodium.org/jedi.gpg.asc
gpg --import jedi.gpg.asc
gpg --verify libsodium-1.0.3.tar.gz.sig libsodium-1.0.3.tar.gz
tar zxvf libsodium-1.0.3.tar.gz
cd libsodium-1.0.3
./configure; make check
sudo make install
sudo ldconfig
&lt;/code&gt;&lt;/pre&gt;

&lt;h5 id=&#34;zeromq&#34;&gt;zeromq&lt;/h5&gt;

&lt;p&gt;Next up, we build ZeroMQ with libsodium support:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;git clone git@github.com:zeromq/libzmq.git
cd libzmq
./autogen.sh
./configure --with-libsodium
make check
sudo make install
sudo ldconfig
&lt;/code&gt;&lt;/pre&gt;

&lt;h5 id=&#34;czmq&#34;&gt;czmq&lt;/h5&gt;

&lt;p&gt;Now, we&amp;rsquo;ll build CZMQ against ZeroMQ. For an overview of what the CZMQ API provides, see the &lt;a href=&#34;http://api.zeromq.org/czmq3-0:_start&#34;&gt;reference manual&lt;/a&gt;.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;git clone git@github.com/zeromq/czmq.git
cd czmq
./autogen.sh
./configure
make check
sudo make install
sudo ldconfig
&lt;/code&gt;&lt;/pre&gt;

&lt;h5 id=&#34;goczmq&#34;&gt;goczmq&lt;/h5&gt;

&lt;p&gt;Now, finally we can build GoCZMQ itself:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;mkdir -p $GOPATH/src/github.com/zeromq
cd $GOPATH/src/github.com/zeromq
git clone git@github.com/zeromq/goczmq.git
cd goczmq
go test -v
go install
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;For an overview of the GoCZMQ API, see the &lt;a href=&#34;http://godoc.org/github.com/zeromq/goczmq&#34;&gt;godoc.org&lt;/a&gt;&lt;/p&gt;

&lt;h4 id=&#34;hello-world&#34;&gt;Hello World&lt;/h4&gt;

&lt;p&gt;No &amp;ldquo;getting started&amp;rdquo; post is complete without the requisite &amp;ldquo;hello world&amp;rdquo; example. In this example, we&amp;rsquo;ll connect a &amp;ldquo;push&amp;rdquo; socket to a &amp;ldquo;pull&amp;rdquo; socket in the same process, and send a message. This is probably not something we&amp;rsquo;d do in a real use case, but it&amp;rsquo;s a nice demonstration of how ZeroMQ is asyncronous.&lt;/p&gt;

&lt;h5 id=&#34;helloworld-go&#34;&gt;helloworld.go&lt;/h5&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt;package main

import (
	&amp;quot;fmt&amp;quot;

	&amp;quot;github.com/zeromq/goczmq&amp;quot;
)

func main() {
	push, err := goczmq.NewPush(&amp;quot;tcp://127.0.0.1:31337&amp;quot;)
	if err != nil {
		panic(err)
	}

	pull, err := goczmq.NewPull(&amp;quot;tcp://127.0.0.1:31337&amp;quot;)
	if err != nil {
		panic(err)
	}

	err = push.SendFrame([]byte(&amp;quot;Hello World&amp;quot;), goczmq.FlagNone)
	if err != nil {
		panic(err)
	}

	frame, sz, err := pull.RecvFrame()
	if err != nil {
		panic(err)
	}

	fmt.Printf(&amp;quot;We received a message of size %d\n&amp;quot;, sz)
	fmt.Printf(&amp;quot;The message was: &#39;%s&#39;\n&amp;quot;, frame)

	pull.Destroy()
	push.Destroy()
}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Let&amp;rsquo;s go over what is happening in this short example in detail. First, we create a ZMQ_PUSH socket:&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt; 	push, err := goczmq.NewPush(&amp;quot;tcp://127.0.0.1:31337&amp;quot;)
	if err != nil {
		panic(err)
	}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The NewPush function does quite a bit for us under the hood. Since it&amp;rsquo;s the first socket we&amp;rsquo;ve created in this program, it creates a ZeroMQ context for us. Then, it creates the socket and starts trying to connect the socket to the endpoint.&lt;/p&gt;

&lt;p&gt;Note that we&amp;rsquo;ve constructed a TCP socket that is connecting before we&amp;rsquo;ve created the bound socket! With ZeroMQ, the order of bind and connect calls does not matter. The socket will retry connecting in the background until it is successful.&lt;/p&gt;

&lt;p&gt;Next, we&amp;rsquo;ll construct a ZMQ_PULL socket which will bind to the endpoint. After it is bound, the ZMQ_PUSH socket will successfully connect to it.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt;	pull, err := goczmq.NewPull(&amp;quot;tcp://*:31337&amp;quot;)
	if err != nil {
		panic(err)
	}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now comes the exciting part - sending a message! In &lt;a href=&#34;http://rfc.zeromq.org/spec:23&#34;&gt;ZMTP&lt;/a&gt; (the protocol implemented by ZeroMQ), a &amp;ldquo;message&amp;rdquo; consists of one or more byte agnostic frames. We will send a message by using the SendFrame API call. SendFrame accepts a []byte followed by flags. In this case, FlagNone indicates there are no frames following this frame, so the single frame should be treated as a full message. If this frame were the first frame in multi-part message, we would use goczmq.FlagMore.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt;	err = push.SendFrame([]byte(&amp;quot;Hello World&amp;quot;), goczmq.FlagNone)
	if err != nil {
		panic(err)
	}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;An important detail to note here is that ZeroMQ is asyncronous and provides a buffer under the hood - so our program execution continues after the call to SendFrame even though the message has not yet been received. This is what allows this example to send and receive in the same thread.&lt;/p&gt;

&lt;p&gt;Now it&amp;rsquo;s time to receive the message:&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt;	frame, sz, err := pull.RecvFrame()
	if err != nil {
		panic(err)
	}

	fmt.Printf(&amp;quot;We received a message of size %d\n&amp;quot;, sz)
	fmt.Printf(&amp;quot;The message was: &#39;%s&#39;\n&amp;quot;, frame)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;After we&amp;rsquo;re done with the sockets, we call sock.Destroy to clean up memory. While Go is a garbage collected language, we&amp;rsquo;re wrapping a C API, so we need to clean up after ourselves:&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-go&#34;&gt;	pull.Destroy()
	push.Destroy()
&lt;/code&gt;&lt;/pre&gt;

&lt;h4 id=&#34;next-steps&#34;&gt;Next Steps&lt;/h4&gt;

&lt;p&gt;ZeroMQ is a large topic, and ZeroMQ combined with Go doubly so. I&amp;rsquo;ll be working on a series of articles covering the usage of interesting parts of the GoCZMQ API as well as lessons learned from working on the project.&lt;/p&gt;

&lt;h4 id=&#34;resources&#34;&gt;Resources&lt;/h4&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;http://github.com/zeromq/goczmq&#34;&gt;GoCZMQ&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;http://godoc.org/github.com/zeromq/goczmq&#34;&gt;GoCZMQ Documentation&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;http://api.zeromq.org/czmq3-0:_start&#34;&gt;CZMQ Documentation&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;http://rfc.zeromq.org/spec:22&#34;&gt;The Collective Code Construction Contract&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://github.com/zeromq/&#34;&gt;The ZeroMQ Organization on Github&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</description>
    </item>
    
    <item>
      <title>Problem: I Want To Write To Redis From Rsyslog</title>
      <link>http://taotetek.github.io/oldschool.systems/post/omhiredis/</link>
      <pubDate>Sun, 20 Sep 2015 07:54:33 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/omhiredis/</guid>
      <description>

&lt;h3 id=&#34;solution-use-the-omhiredis-output-plugin&#34;&gt;Solution: Use the omhiredis output plugin.&lt;/h3&gt;

&lt;p&gt;I recently saw &lt;a href=&#34;https://twitter.com/radu0gheorghe&#34;&gt;Radu Gheorghe&lt;/a&gt; express an interest in feeding &lt;a href=&#34;https://www.elastic.co/products/logstash&#34;&gt;Logstash&lt;/a&gt; from &lt;a href=&#34;http://rsyslog.com/&#34;&gt;Rsyslog&lt;/a&gt; via a &lt;a href=&#34;http://redis.io&#34;&gt;Redis&lt;/a&gt; queue. I realized that I&amp;rsquo;d neglected the &lt;a href=&#34;https://github.com/rsyslog/rsyslog/tree/master/contrib/omhiredis&#34;&gt;omhiredis&lt;/a&gt; plugin since I&amp;rsquo;d original written it as a proof of concept. This seemed like a good opportunity to revisit the plugin and start fixing it up. For the rsyslog 8.13 release I added support for &lt;a href=&#34;http://redis.io/commands/lpush&#34;&gt;LPUSH&lt;/a&gt; and &lt;a href=&#34;http://redis.io/commands/publish&#34;&gt;PUBLISH&lt;/a&gt;.&lt;/p&gt;

&lt;h2 id=&#34;building&#34;&gt;Building&lt;/h2&gt;

&lt;h3 id=&#34;requirements&#34;&gt;Requirements&lt;/h3&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;http://www.rsyslog.com/&#34;&gt;Rsyslog 8.13&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://github.com/redis/hiredis&#34;&gt;Hiredis&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;Configure rsyslog with omhiredis support:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;~/git/rsyslog&amp;gt; ./configure --enable-omhiredis &amp;lt;other options&amp;gt;
~/git/rsyslog&amp;gt; make
~/git/rsyslog&amp;gt; sudo make install
&lt;/code&gt;&lt;/pre&gt;

&lt;h2 id=&#34;usage&#34;&gt;Usage&lt;/h2&gt;

&lt;h3 id=&#34;queue-mode&#34;&gt;Queue Mode&lt;/h3&gt;

&lt;p&gt;In queue mode, omhiredis will LPUSH each message into a Redis list stored at the defined key. You may define an optional template using the &amp;ldquo;template&amp;rdquo; parameter. If a template is not defined, the action will default to RSYSLOG_ForwardFormat.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;module(load=&amp;quot;omhiredis&amp;quot;)

action(
  name=&amp;quot;push_redis&amp;quot;
  type=&amp;quot;omhiredis&amp;quot;
  mode=&amp;quot;queue&amp;quot;
  key=&amp;quot;testqueue&amp;quot;
)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Here is an example of RPOPing a log line from Redis that was inserted using the above action:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;&amp;gt; redis-cli 
127.0.0.1:6379&amp;gt; RPOP my_queue

&amp;quot;&amp;lt;46&amp;gt;2015-09-17T10:54:50.080252-04:00 myhost rsyslogd: [origin software=\&amp;quot;rsyslogd\&amp;quot; swVersion=\&amp;quot;8.13.0.master\&amp;quot; x-pid=\&amp;quot;6452\&amp;quot; x-info=\&amp;quot;http://www.rsyslog.com\&amp;quot;] start&amp;quot;
127.0.0.1:6379&amp;gt; 
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;publish-mode&#34;&gt;Publish Mode&lt;/h3&gt;

&lt;p&gt;In publish mode, omhiredis will PUBLISH each message onto a Redis channel stored at the defined key. Like queue mode, you may define a template using the &amp;ldquo;template&amp;rdquo; parameter, and if not set the template defaults to RSYSLOG_ForwardFormat.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;module(load=&amp;quot;omhiredis&amp;quot;)

action(
  name=&amp;quot;push_redis&amp;quot;
  type=&amp;quot;omhiredis&amp;quot;
  mode=&amp;quot;queue&amp;quot;
  key=&amp;quot;testqueue&amp;quot;
)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Here is an example of subscribing to the channel defined in the above template, and receiving a published message from it:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;&amp;gt; redis-cli 
127.0.0.1:6379&amp;gt; subscribe my_channel

Reading messages... (press Ctrl-C to quit)

1) &amp;quot;subscribe&amp;quot;

2) &amp;quot;my_channel&amp;quot;

3) (integer) 1

1) &amp;quot;message&amp;quot;

2) &amp;quot;my_channel&amp;quot;

3) &amp;quot;&amp;lt;46&amp;gt;2015-09-17T10:55:44.486416-04:00 myhost rsyslogd-pstats: {\&amp;quot;name\&amp;quot;:\&amp;quot;imuxsock\&amp;quot;,\&amp;quot;origin\&amp;quot;:\&amp;quot;imuxsock\&amp;quot;,\&amp;quot;submitted\&amp;quot;:0,\&amp;quot;ratelimit.discarded\&amp;quot;:0,\&amp;quot;ratelimit.numratelimiters\&amp;quot;:0}&amp;quot;
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;template-mode&#34;&gt;Template Mode&lt;/h3&gt;

&lt;p&gt;In template mode, omhiredis will send the message constructed by the template directly to Redis as a command. Originally, this was the only mode the plugin supported. Please note that there is an outstanding bug in this mode - it will not properly handle commands with spaces in the message payload.  For example, manually constructing an LPUSH of a full message using template mode will not work properly. I hope to find the time to fix this in the future. Pull requests are of course accepted!&lt;/p&gt;

&lt;p&gt;Here&amp;rsquo;s an example of keeping a tally of the number of messages seen by program name. Note that mode is not set, as it&amp;rsquo;s the default mode for this module. Additionally, there&amp;rsquo;s a config parsing bug that is triggered if you explicitly set it, which will be fixed in 8.14.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;module(load=&amp;quot;omhiredis&amp;quot;)

template(
  name=&amp;quot;program_count_tmpl&amp;quot;
  type=&amp;quot;string&amp;quot;
  string=&amp;quot;HINCRBY progcount %programname% 1&amp;quot;
)

action(
  name=&amp;quot;count_programs&amp;quot;
  server=&amp;quot;my-redis-server.example.com&amp;quot;
  port=&amp;quot;6379&amp;quot;
  type=&amp;quot;omhiredis&amp;quot;
  template=&amp;quot;program_count_tmpl&amp;quot;
)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Here, we take a look at the counts stored in Redis:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;&amp;gt; redis-cli 
127.0.0.1:6379&amp;gt; HGETALL progcount
1) &amp;quot;rsyslogd&amp;quot;
2) &amp;quot;35&amp;quot;
3) &amp;quot;rsyslogd-pstats&amp;quot;
4) &amp;quot;4302&amp;quot;
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;pipelining-with-queue-dequeuebatchsize&#34;&gt;Pipelining with queue.dequeuebatchsize&lt;/h3&gt;

&lt;p&gt;The omhiredis plugin supports &lt;a href=&#34;http://redis.io/topics/pipelining/&#34;&gt;pipelining&lt;/a&gt; using Rsyslog queuing with the queue.dequeuebatchsize. Note that the plugin does not currently check for errors in the replies. If the plugin becomes something people are seriously interested in, error handling should definitely be added. For those interested in internals, here&amp;rsquo;s the current end transaction block in the code:&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-c&#34;&gt;BEGINendTransaction
CODESTARTendTransaction
    dbgprintf(&amp;quot;omhiredis: endTransaction called\n&amp;quot;);
    int i;
    pWrkrData-&amp;gt;replies = malloc ( sizeof ( redisReply* ) * pWrkrData-&amp;gt;count );
    for ( i = 0; i &amp;lt; pWrkrData-&amp;gt;count; i++ ) {
        redisGetReply ( pWrkrData-&amp;gt;conn, (void *)&amp;amp;pWrkrData-&amp;gt;replies[i] );
        /*  TODO: add error checking here! */
        freeReplyObject ( pWrkrData-&amp;gt;replies[i] );
    }
    free ( pWrkrData-&amp;gt;replies );
ENDendTransaction
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The rest of the source may be viewed on &lt;a href=&#34;https://github.com/rsyslog/rsyslog/blob/master/contrib/omhiredis/omhiredis.c&#34;&gt;github&lt;/a&gt;.&lt;/p&gt;

&lt;h2 id=&#34;future-plans&#34;&gt;Future Plans&lt;/h2&gt;

&lt;p&gt;I believe the next steps for this plugin should be:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Fixing &amp;ldquo;template&amp;rdquo; mode so that message payloads containing spaces work properly.&lt;/li&gt;
&lt;li&gt;Error handling to prevent message loss on redis failure.&lt;/li&gt;
&lt;li&gt;A complimentary &amp;ldquo;imhiredis&amp;rdquo; plugin that can receive from Redis&lt;/li&gt;
&lt;/ul&gt;
</description>
    </item>
    
    <item>
      <title>Problem: We (Software Developers) Attack The Work and Energy Of Others</title>
      <link>http://taotetek.github.io/oldschool.systems/post/egos/</link>
      <pubDate>Sat, 19 Sep 2015 19:13:55 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/egos/</guid>
      <description>

&lt;h3 id=&#34;solution-be-aware-of-this-pattern-and-don-t-participate-in-it&#34;&gt;Solution: Be aware of this pattern and don&amp;rsquo;t participate in it.&lt;/h3&gt;

&lt;p&gt;There is a destructive pattern that I see often among free software developers. It expresses itself through attacks on the work of others. As far as I can tell, the impedus behind it is a desire to secure status by expressing the idea that the work of others is contemptable. People will enter into this pattern together, reinforcing the idea that they are in the high status group while deriding the efforts of others.&lt;/p&gt;

&lt;p&gt;In my own experience observing this pattern, the targets of these attacks are usually people involved with communities around technologies that are relatively welcoming to novices.  The fact that these communities have produced impressive feats of software requiring deep expertise and domain knowledge is ignored in favor of belittling technology choices. Often, the flaws of the technology used will be exaggerated, giving the impression that it is &amp;ldquo;impossible&amp;rdquo; to create anything of value using it. The attitude projected is usually one of aloof, jaded cynicism. The posture is usually &amp;ldquo;playing to the crowd&amp;rdquo;.&lt;/p&gt;

&lt;p&gt;Unfortunately, this behavior is especially damaging when aimed at novices, as they may lack the foundation of confidence necessary to keep from internalizing the idea that the knowledge they have aquired so far lacks value. These attacks plant seeds of doubt and undermine the learning process.&lt;/p&gt;

&lt;p&gt;Maximizing the set of problems solved by software and finding the most accurate solution for each problem requires diverse participants with differing (and often conflicting) perspectives. Attacks on novices as well as attacks on novel approaches to software development help reinforce monocultures which severely limit the inputs into collaborative processes, which in turn severely limits the potential outputs. This pattern is anti-social, and puts the ego and status of individuals over the goals of collaboration, problem solving, and knowledge creation.&lt;/p&gt;

&lt;p&gt;Since human beings engage in patterns of behavior because the behavior is rewarded, the solution to this problem is to create high status organizations with cultures that do not reward these behaviors. As individuals a simple step we can take is to not participate in nor reward others who participate in this behavior, and to point it out for what it is when we see it.&lt;/p&gt;

&lt;p&gt;There are too many problems that need to be solved to waste time and energy deliberately doing damage to individuals and communities who are actively engaged in problem solving.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Problem: Starting Each Post With &#39;Problem&#39; May Seem Odd</title>
      <link>http://taotetek.github.io/oldschool.systems/post/problems/</link>
      <pubDate>Sat, 19 Sep 2015 17:31:32 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/problems/</guid>
      <description>

&lt;h3 id=&#34;solution-explain-why-we-re-doing-this&#34;&gt;Solution: Explain why we&amp;rsquo;re doing this.&lt;/h3&gt;

&lt;p&gt;One of my favorite free software communities that I participate in is &lt;a href=&#34;https://github.com/zeromq&#34;&gt;the ZeroMQ project&lt;/a&gt;. This is a distributed organization of programmers working on a software ecosystem around &lt;a href=&#34;https://zeromq.org&#34;&gt;ZeroMQ&lt;/a&gt;, and using it to solve different problems. We develop software within this organization using a process called the &lt;a href=&#34;http://rfc.zeromq.org/spec:22&#34;&gt;Collective Code Construction Contract&lt;/a&gt;. This is a software development process designed with the goal of maximizing the scale of community around a project. If you&amp;rsquo;re curious as to how well it&amp;rsquo;s working, I suggest reading &lt;a href=&#34;http://hintjens.com/blog:93&#34;&gt;&amp;ldquo;C4.1 - an Exothermic Process&amp;rdquo;&lt;/a&gt; by the ZeroMQ organization&amp;rsquo;s benevolent dictator, Pieter Hintjens.&lt;/p&gt;

&lt;p&gt;One of the core practices we use within this methodology is limiting patches to code to the scope of solving a narrowly defined problem. Solving more than one problem with a patch is cheating. If a patch identifies a problem, solves it, and meets some basic style guidelines, it gets accepted. Additionally, the author of the patch is made a new maintainer within the organization. We do not believe in high barriers to entry to our organization, as it would be counter to the goal of maximizing the scale of the community.&lt;/p&gt;

&lt;p&gt;I have found getting into the mindset of identifying individual problems and solving them minimally and accurately to be a wonderful tool for maintaining focus. So, I thought it would be fun to try this technique on my blog as well. If I&amp;rsquo;m expending the effort to write, there most be a reason I&amp;rsquo;m doing so. So, even if the problem I&amp;rsquo;m addressing is &amp;ldquo;I want to look smart about a subject&amp;rdquo;, or &amp;ldquo;my ego likes it when people read things I write&amp;rdquo;, I&amp;rsquo;m still going to identify the problem each post is addressing. I think it will be fun.&lt;/p&gt;

&lt;p&gt;If you&amp;rsquo;d like to learn more about the C4 process (it&amp;rsquo;s actually the C4.1 process now, as we patched it) I highly recommend watching the talk &lt;a href=&#34;https://vimeo.com/79378301&#34;&gt;&amp;ldquo;Building consistently good software with ordinary people&amp;rdquo;&lt;/a&gt;. It&amp;rsquo;s one of my favorite talks about collaboration.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Problem: There Are No Posts</title>
      <link>http://taotetek.github.io/oldschool.systems/post/hello/</link>
      <pubDate>Sat, 19 Sep 2015 11:16:18 -0400</pubDate>
      
      <guid>http://taotetek.github.io/oldschool.systems/post/hello/</guid>
      <description>

&lt;h3 id=&#34;solution-write-a-hello-world-post&#34;&gt;Solution: Write a &amp;lsquo;Hello World&amp;rsquo; post.&lt;/h3&gt;

&lt;p&gt;Greetings, and welcome to the Oldschool System blog. Thank you so much for taking the time to stop by here and read my thoughts. I&amp;rsquo;m flattered, and will endeavor to write things that are worth your time.&lt;/p&gt;

&lt;p&gt;The sorts of things you are most likely to find here are:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Information about the free software projects I participate in&lt;/li&gt;
&lt;li&gt;Examples of how to do things I find fun with programming languages&lt;/li&gt;
&lt;li&gt;Thoughts on the culture(s) that have formed around software engineering&lt;/li&gt;
&lt;li&gt;Political musings, mostly around the intersection of my craft and politics&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;If these are things that might interest you for some reason, check in now and then. If these are things that don&amp;rsquo;t seem very interesting, no offense taken, and enjoy the rest of your journey.&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>