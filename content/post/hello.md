+++
Categories = ["thoughts"]
Description = "Welcome to oldschool.systems"
Tags = ["taotetek"]
date = "2015-09-19T11:16:18-04:00"
title = "Problem: There Are No Posts"

+++
### Solution: Write a 'Hello World' post.

Greetings, and welcome to the Oldschool System blog. Thank you so much for taking the time to stop by here and read my thoughts. I'm flattered, and will endeavor to write things that are worth your time.

The sorts of things you are most likely to find here are:

* Information about the free software projects I participate in
* Examples of how to do things I find fun with programming languages
* Thoughts on the culture(s) that have formed around software engineering
* Political musings, mostly around the intersection of my craft and politics

If these are things that might interest you for some reason, check in now and then. If these are things that don't seem very interesting, no offense taken, and enjoy the rest of your journey.
