+++
Categories = ["thoughts"]
Description = "Thoughts on the death of a friend"
Tags = ["community","zerom"]
date = "2016-04-30T22:35:54-04:00"
title = "Problem: My Friend Pieter is Dying"

+++
### Solution: There's no solution.
My friend Pieter Hintjens [is dying](http://hintjens.com/blog:115). I'm going to miss him. It's a truth in life that sometimes we are presented with problems that cannot be solved. This is one of them. Pieter is now facing the ultimate unsolvable problem - the one that we will all eventually face. I'm facing the problem of navigating a storming sea of thoughts and emotions that I'm experiencing as a person who knows him, and values his presence in my life, and is facing losing that presence. None of these thoughts and emotions really fit into words for me. This is a constant and different problem in my life - words always feel too small to me and stringing them together into sentences is so linear, slow, and inadequate. 

Pieter, I'll always carry you with me. All of the colors and textures of the too few moments we got to spend together. Stumbling drunk through London within hours of conversation and laughter. Discussing ideas for ZeroMQ and thoughts about psycopathy in Brussels. Having the pleasure of introducing you to various friends. Music in the warehouse.

It was good, my friend. Every moment was good. Thank you for the moments shared. All of them are kept, and treasured. Your gestures will carry their momentum beyond you through those who understand what you were trying to say with ZeroMQ, and we'll keep showing others. 

It has been an honor and a rare pleasure.
