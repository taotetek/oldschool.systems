+++
Categories = ["thoughts"]
Description = "About the Collective Code Construction Contract"
Tags = ["community","c4","zeromq"]
date = "2015-09-19T17:31:32-04:00"
title = "Problem: Starting Each Post With 'Problem' May Seem Odd"

+++
### Solution: Explain why we're doing this.

One of my favorite free software communities that I participate in is [the ZeroMQ project](https://github.com/zeromq). This is a distributed organization of programmers working on a software ecosystem around [ZeroMQ](https://zeromq.org), and using it to solve different problems. We develop software within this organization using a process called the [Collective Code Construction Contract](http://rfc.zeromq.org/spec:22). This is a software development process designed with the goal of maximizing the scale of community around a project. If you're curious as to how well it's working, I suggest reading ["C4.1 - an Exothermic Process"](http://hintjens.com/blog:93) by the ZeroMQ organization's benevolent dictator, Pieter Hintjens.

One of the core practices we use within this methodology is limiting patches to code to the scope of solving a narrowly defined problem. Solving more than one problem with a patch is cheating. If a patch identifies a problem, solves it, and meets some basic style guidelines, it gets accepted. Additionally, the author of the patch is made a new maintainer within the organization. We do not believe in high barriers to entry to our organization, as it would be counter to the goal of maximizing the scale of the community.

I have found getting into the mindset of identifying individual problems and solving them minimally and accurately to be a wonderful tool for maintaining focus. So, I thought it would be fun to try this technique on my blog as well. If I'm expending the effort to write, there most be a reason I'm doing so. So, even if the problem I'm addressing is "I want to look smart about a subject", or "my ego likes it when people read things I write", I'm still going to identify the problem each post is addressing. I think it will be fun. 

If you'd like to learn more about the C4 process (it's actually the C4.1 process now, as we patched it) I highly recommend watching the talk ["Building consistently good software with ordinary people"](https://vimeo.com/79378301). It's one of my favorite talks about collaboration.
