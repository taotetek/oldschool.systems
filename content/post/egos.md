+++
Categories = ["thoughts"]
Description = "Thoughts on a culture of negativity"
Tags = ["community"]
date = "2015-09-19T19:13:55-04:00"
title = "Problem: We (Software Developers) Attack The Work and Energy Of Others"

+++
### Solution: Be aware of this pattern and don't participate in it.

There is a destructive pattern that I see often among free software developers. It expresses itself through attacks on the work of others. As far as I can tell, the impedus behind it is a desire to secure status by expressing the idea that the work of others is contemptable. People will enter into this pattern together, reinforcing the idea that they are in the high status group while deriding the efforts of others.

In my own experience observing this pattern, the targets of these attacks are usually people involved with communities around technologies that are relatively welcoming to novices.  The fact that these communities have produced impressive feats of software requiring deep expertise and domain knowledge is ignored in favor of belittling technology choices. Often, the flaws of the technology used will be exaggerated, giving the impression that it is "impossible" to create anything of value using it. The attitude projected is usually one of aloof, jaded cynicism. The posture is usually "playing to the crowd".

Unfortunately, this behavior is especially damaging when aimed at novices, as they may lack the foundation of confidence necessary to keep from internalizing the idea that the knowledge they have aquired so far lacks value. These attacks plant seeds of doubt and undermine the learning process.

Maximizing the set of problems solved by software and finding the most accurate solution for each problem requires diverse participants with differing (and often conflicting) perspectives. Attacks on novices as well as attacks on novel approaches to software development help reinforce monocultures which severely limit the inputs into collaborative processes, which in turn severely limits the potential outputs. This pattern is anti-social, and puts the ego and status of individuals over the goals of collaboration, problem solving, and knowledge creation.

Since human beings engage in patterns of behavior because the behavior is rewarded, the solution to this problem is to create high status organizations with cultures that do not reward these behaviors. As individuals a simple step we can take is to not participate in nor reward others who participate in this behavior, and to point it out for what it is when we see it.

There are too many problems that need to be solved to waste time and energy deliberately doing damage to individuals and communities who are actively engaged in problem solving.
