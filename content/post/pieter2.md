+++
Categories = ["thoughts"]
Description = "Thoughts on missing a friend"
Tags = ["community","zeromq"]
date = "2016-10-09T08:56:11-04:00"
title = "Problem: My Friend Pieter is Dead"
+++
### Solution: Carry his ideas forward.
It was March of 2016 when I got the news from Pieter that his cancer was back, and that it was terminal this time. Shortly after that in April, he wrote ["A Protocol for Dying"](http://hintjens.com/blog:115) and announced it to the world. He lived much longer than was first expected, dying on October 4th on ["his own terms"](http://hintjens.com/blog:116) once cancer made it impossible for him to live on his own terms. I am left with grief, wonderful memories, and membership in an amazing community of software developers that Pieter built.

I want to write some about the ideals that Pieter held that I also share - most importantly I think, his fierce dedication to taking power away from gatekeepers, and his belief in the power of collective intelligence. Right now, I'm still too sad and tired to muster up the energy required - but I felt the need to write the bookend to my April post.

If you want to know what Pieter stood for, read his books. You can find them for free on the internet, but if you can afford to buy them please do - the money from the sales will help support his three children.

* ["Culture and Empire"](https://www.amazon.com/Culture-Empire-Digital-Revolution-1/dp/1492999776)
* ["The Psycopath Code"](https://www.amazon.com/Psychopath-Code-Cracking-Predators-Stalk/dp/1514342022/ref=sr_1_1?s=books&ie=UTF8&qid=1476019689&sr=1-1&keywords=the+psychopath+code)
* ["Social Architecture"](https://www.amazon.com/Social-Architecture-Building--line-Communities/dp/1533112452/ref=sr_1_1?s=books&ie=UTF8&qid=1476019722&sr=1-1&keywords=social+architecture)
* ["Confessions of a Necromancer"](https://www.amazon.com/Confessions-Necromancer-stories-Pieter-Hintjens/dp/1539178846/ref=sr_1_2?s=books&ie=UTF8&qid=1476019750&sr=1-2&keywords=confessions+of+a+necromancer)






