+++
Categories = ["about me"]
Description = ""
Tags = []
date = "2015-09-19T10:45:39-04:00"
title = "Problem: People don't know who I am"

+++

### Solution: Make an 'About' page.

## A Quote I Like
> "FYI man, alright. You could sit at home, and do like absolutely nothing, and your name goes through like 17 computers a day. 1984? Yeah right, man. That's a typo. Orwell is here now. He's livin' large. We have no names, man. No names. We are nameless!" - Cereal Killer (from the movie "Hackers")

## Projects
* [GoCZMQ: Golang ZeroMQ API (Original Author)](https://github.com/zeromq/goczmq/)
* [Rsyslog: Rocket Fast Logging (Contributor)](https://github.com/rsyslog//rsyslog/)
* [CZMQ: C ZeroMQ API (Contributor)](https://github.com/zeromq/czmq/)
* [Zproto: ZeroMQ Protocol Generator (Contributor)](https://github.com/zeromq/zproto/)

## Find Me
* [Github](https://github.com/taotetek/)
* [Twitter](https://twitter.com/taotetek/)
* [Gopher Slack](https://gophers.slack.com/)
* [GoBridge Forums](https://forum.golangbridge.org/)

![me](https://oldschool.systems/images/me.gif)
